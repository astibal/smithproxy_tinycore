echo "=== Fixing autotools symlinks"

ln -s /usr/local/bin/aclocal /usr/local/bin/aclocal-1.14
ln -s /usr/local/bin/automake /usr/local/bin/automake-1.14
ln -s /usr/local/bin/autoconf /usr/local/bin/autoconf-1.14


echo "=== LIBCONFIG ==="

cd libconfig
rm -f lib/scanner.c lib/scanner.h
./configure
sudo make install
cd ../

echo "=== LIBCLI ==="

cd libcli
sudo make install
cd ../
