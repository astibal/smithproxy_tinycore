#!/bin/sh

REMOTE='tc@192.168.122.59'
SX_DIR='/home/astib/pro/smithproxy_src/'
DEP_DIR='smithproxy-deps'
TCPATCH_DIR='tcpatches'

# =======================

rsync -avr ${SX_DIR}      --exclude '*.o' --exclude '*CMakeFiles*' --exclude '*CMakeCache.txt*' --exclude '*.a' --exclude 'archives/*' --exclude 'cmake_install.cmake'  --exclude 'Makefile' --exclude 'socle_examples' ${REMOTE}:/tmp/smithproxy-build/
rsync -avr ${DEP_DIR}     --exclude '*.o' --exclude '*CMakeFiles*' --exclude '*CMakeCache.txt*' --exclude '*.a' --exclude 'archives/*' --exclude 'cmake_install.cmake'  --exclude 'master_*' ${REMOTE}:/tmp/smithproxy-build/
rsync -avr ${TCPATCH_DIR} --exclude '*.o' --exclude '*CMakeFiles*' --exclude '*CMakeCache.txt*' --exclude '*.a' --exclude 'archives/*' --exclude 'cmake_install.cmake'  --exclude 'master_*' ${REMOTE}:/tmp/smithproxy-build/
rsync -avr make*          --exclude '*.o' --exclude '*CMakeFiles*' --exclude '*CMakeCache.txt*' --exclude '*.a' --exclude 'archives/*' --exclude 'cmake_install.cmake'  --exclude 'master_*' ${REMOTE}:/tmp/smithproxy-build/
