#!/bin/sh

P=`pwd`


echo "====== Building smithproxy dependencies ====="

cd smithproxy-deps/
sudo ./make_install.sh

echo "===== Patching smithproxy ====="
cd $P

echo "     .... libunwind not supported"
cd smithproxy/
	patch -p1  <  ../tcpatches/no_unwind_lib.patch

	echo "===== Library symlinks ====="
	sudo ln -s /usr/local/lib/libconfig++.so.9 /usr/lib/libconfig++.so.9
	sudo ln -s /usr/local/lib/libcli.so.1.9 /usr/lib/libcli.so.1.9

	echo "===== Build smithproxy ====="

	cd ./build/ && cmake .. && sudo make install

echo "===== All done ====="
cd $P

